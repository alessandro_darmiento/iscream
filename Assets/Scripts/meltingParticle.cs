﻿//La dimensione in X e Z delle particelle emesse viene settata impostando il parametro scale X e scale Z del GameObject padre, ovvero il MeltingParticleGravity
//Il raggio del cerchio emettitore di particelle viene regolato impostando il parametro scale X del GameObjectPadre, ovvero il MeltingParticleGravity

using UnityEngine;
using System.Collections;

public class meltingParticle : MonoBehaviour {
	public Color meltingParticleColor=Color.red;
	public float playerHealth=1;	//indica la vita del personaggio normalizzata all'intervallo [0,1]
	public int playerSpeed=1;	//indica la velocità del personaggio non normalizzata
	private ParticleSystem[] meltingEffect;	//array contenente i riferimenti ai particellari figli
	private float[] meltingParticle_Drop_MaxEmissionRate;	//indica la massima emissione di un particellare figlio (in pratica indica l'emissione quando il gelato è al minimo di vita), va settata direttamente nell'inspector


	void Start () {
		//recupero i riferimenti ai particellari figli
		meltingEffect=GetComponentsInChildren<ParticleSystem>();
		meltingParticle_Drop_MaxEmissionRate=new float[meltingEffect.Length];
		//per ogni particellare figlio
		for(int a=0; a<meltingEffect.Length; a++) {
			//cambio il colore
			meltingEffect[a].startColor=meltingParticleColor;
			//regolo la dimensione in X e Z delle particelle emesse
			meltingEffect[a].transform.localScale=new Vector3(transform.lossyScale.x,meltingEffect[a].transform.localScale.y,transform.lossyScale.z);
			//regolo il raggio del cerchio emettitore
			ParticleSystem.ShapeModule shapeModule=meltingEffect[a].shape;
			shapeModule.radius=meltingEffect[a].shape.radius*transform.lossyScale.x;
			//recupero la massima emissione
			meltingParticle_Drop_MaxEmissionRate[a]=meltingEffect[a].emission.rate.constant;
			//regolo l'emissione in base alla vita del personaggio (meno vita => più emissione)
			ParticleSystem.EmissionModule emissionModule=meltingEffect[a].emission;
			ParticleSystem.MinMaxCurve emissionRate=emissionModule.rate;
			if(playerHealth!=0) emissionRate.constant=meltingParticle_Drop_MaxEmissionRate[a]*(1-playerHealth);
			else emissionRate.constant=0;
			emissionModule.rate=emissionRate;
			//regolo la forza in direzione -zGlobal applicata alle particelle (partendo da -10 a velocità 1, dopodichè ogni incremento unitario di playerSpeed aggiunge -5)
			ParticleSystem.ForceOverLifetimeModule forceModule=meltingEffect[a].forceOverLifetime;
			forceModule.z=-5-(playerSpeed-1)*5; } }

	void Update () {
		for(int a=0; a<meltingEffect.Length; a++) meltingEffect[a].startColor=meltingParticleColor;
		setEmission();
		setWindStrength(); }

	private void setEmission() {	//regolo l'emissione in base alla vita del personaggio (meno vita => più emissione)
		for(int a=0; a<meltingEffect.Length; a++) {
			ParticleSystem.EmissionModule emissionModule=meltingEffect[a].emission;
			ParticleSystem.MinMaxCurve emissionRate=emissionModule.rate;
			if(playerHealth!=0) emissionRate.constant=meltingParticle_Drop_MaxEmissionRate[a]*(1-playerHealth)*(1-playerHealth);
			else emissionRate.constant=0;
			emissionModule.rate=emissionRate; } }

	private void setWindStrength() {	//regolo la forza in direzione -zGlobal applicata alle particelle in funzione della velocità del personaggio (partendo da -10 a velocità 1, dopodichè ogni incremento unitario di playerSpeed aggiunge -5)
		for(int a=0; a<meltingEffect.Length; a++) {
			ParticleSystem.ForceOverLifetimeModule forceModule=meltingEffect[a].forceOverLifetime;
			forceModule.z=-5-(playerSpeed-1)*5;	} }

	public void setParticlesSize(float size) {
		for(int a=0; a<meltingEffect.Length; a++) {	meltingEffect[a].startSize*=size; }	}
}