﻿using UnityEngine;
using System.Collections;

public class testScript: MonoBehaviour {
	public LayerMask layerMask;
	public GameObject splashParticle;
	private float horizontalSensitivity=3;
	private float verticalSensitivity=3;
	private float verticalRotation=0;

	// Use this for initialization
	void Start () { 
		Cursor.lockState=CursorLockMode.Locked;
		Debug.Log("Metodo Start() frame " + Time.frameCount); }
	
	// Update is called once per frame
	void Update () {
		Debug.DrawRay(transform.position,transform.forward*10,Color.green);
		Debug.DrawRay(Camera.main.ScreenPointToRay(Input.mousePosition).origin,Camera.main.ScreenPointToRay(Input.mousePosition).direction*10,Color.red);
		cameraMovements();
		inputReactor();	}

	private void cameraMovements() {
		Vector3 translationVector;
		//se W premuto muovi avanti
		if(Input.GetKey(KeyCode.W)) {
			if(Input.GetKeyDown(KeyCode.W)) Debug.Log("w pressed (frame "+Time.frameCount+")");
			translationVector=transform.forward;
			translationVector.y=0;
			transform.Translate(translationVector.normalized*Time.deltaTime,Space.World);
			Debug.Log("w (frame "+Time.frameCount+")"); }
		else if(Input.GetKeyUp(KeyCode.W)) Debug.Log("w released (frame "+Time.frameCount+")");
		//se A premuto muovi a sinistra
		if(Input.GetKey(KeyCode.A)) {
			if(Input.GetKeyDown(KeyCode.A)) Debug.Log("a pressed (frame "+Time.frameCount+")");
			translationVector=-transform.right;
			translationVector.y=0;
			transform.Translate(translationVector.normalized*Time.deltaTime,Space.World);
			Debug.Log("a (frame "+Time.frameCount+")"); }
		else if(Input.GetKeyUp(KeyCode.A)) Debug.Log("a released (frame "+Time.frameCount+")");
		//se S premuto muovi indietro
		if(Input.GetKey(KeyCode.S)) {
			if(Input.GetKeyDown(KeyCode.S)) Debug.Log("s pressed (frame "+Time.frameCount+")");
			translationVector=-transform.forward;
			translationVector.y=0;
			transform.Translate(translationVector.normalized*Time.deltaTime,Space.World);
			Debug.Log("s (frame "+Time.frameCount+")"); }
		else if(Input.GetKeyUp(KeyCode.S)) Debug.Log("s released (frame "+Time.frameCount+")");
		//se D premuto muovi a destra
		if(Input.GetKey(KeyCode.D)) {
			if(Input.GetKeyDown(KeyCode.D)) Debug.Log("d pressed (frame "+Time.frameCount+")");
			translationVector=transform.right;
			translationVector.y=0;
			transform.Translate(translationVector.normalized*Time.deltaTime,Space.World);
			Debug.Log("d (frame "+Time.frameCount+")"); }
		else if(Input.GetKeyUp(KeyCode.D)) Debug.Log("d released (frame "+Time.frameCount+")");
		//regola l'orientamento della visuale tramite lo spostamento del mouse
		float h=horizontalSensitivity*100*Input.GetAxis("Mouse X")*Time.deltaTime;
		float v=verticalSensitivity*100*Input.GetAxis("Mouse Y")*Time.deltaTime;
		if(verticalRotation+v>=89) v=89-verticalRotation;
		else if(verticalRotation+v<=-89) v=-verticalRotation-89;
		verticalRotation+=v;
		transform.Rotate(-v,0,0);
		transform.Rotate(0,h,0,Space.World); }

	private void inputReactor() {
		if(Input.GetMouseButtonDown(0)) fireSplash();
		if(Input.GetMouseButtonDown(1)) istanziaCubo();
		if(Input.GetMouseButtonDown(2)) fireHitIndicatorToMousePointer(); }

	private void fireHitIndicatorToNormal() {
		Ray ray=new Ray(transform.position,transform.forward);
		RaycastHit hitInfo;
		if(Physics.Raycast(ray,out hitInfo,1000f,layerMask)) {
			Transform hitIndicatorTransform=((GameObject)Instantiate(Resources.Load<GameObject>("HitIndicator"))).GetComponent<Transform>(); 
			hitIndicatorTransform.position=hitInfo.point;
			hitIndicatorTransform.forward=hitInfo.normal;
			hitIndicatorTransform.parent=hitInfo.transform; } }

	private void istanziaCubo() {
		Transform createdCubeTransformComponent=((GameObject)Instantiate(Resources.Load<GameObject>("CubeOnDemand"))).GetComponent<Transform>(); 
		createdCubeTransformComponent.position=transform.position;
		createdCubeTransformComponent.Translate(transform.forward*6);
		createdCubeTransformComponent.LookAt(transform);
		createdCubeTransformComponent.eulerAngles=new Vector3(0,createdCubeTransformComponent.eulerAngles.y,0);	}

	private void fireHitIndicatorToMousePointer() {
		Ray ray=Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitInfo;
		if(Physics.Raycast(ray,out hitInfo,1000f,layerMask)) {
			Transform hitIndicatorTransform=((GameObject)Instantiate(Resources.Load<GameObject>("HitIndicator"))).GetComponent<Transform>(); 
			hitIndicatorTransform.position=hitInfo.point;
			hitIndicatorTransform.LookAt(ray.origin);
			hitIndicatorTransform.parent=hitInfo.transform; } }

	private void printTexture() {
		Ray ray=new Ray(transform.position,transform.forward);
		RaycastHit hitInfo;
		if(Physics.Raycast(ray,out hitInfo,1000f,layerMask)) {
			Renderer rend=hitInfo.transform.GetComponent<Renderer>();
			MeshCollider meshCollider=hitInfo.collider as MeshCollider;
			if(rend==null) { Debug.Log("Impossibile colorare la mesh, l'oggetto non ha un componente Mesh Renderer!"); return; }
			if(rend.sharedMaterial==null) { Debug.Log("Impossibile colorare la mesh, l'oggetto non possiede un materiale, e pertanto neanche una texture!"); return; }
			if(rend.sharedMaterial.mainTexture==null) { Debug.Log("Impossibile colorare la mesh, l'oggetto possiede un materiale privo di texture!"); return; }
			if(meshCollider==null) { Debug.Log("Impossibile colorare la mesh, l'oggetto non ha un componente Mesh Collider!"); return; }
			if(meshCollider.convex==true) {	Debug.Log("Impossibile colorare la mesh, il Mesh Collider dell'oggetto è impostato su convex!"); return; }
			Texture2D tex=rend.material.mainTexture as Texture2D;
			Vector2 pixelUV=hitInfo.textureCoord;
			pixelUV.x*=tex.width;
			pixelUV.y*=tex.height;
			for(int x=(int)pixelUV.x-2; x<(int)pixelUV.x+2; x++) {
				for(int y=(int)pixelUV.y-5; y<(int)pixelUV.y+5; y++) tex.SetPixel(x,y,Color.black); }
			tex.Apply();
			Debug.Log("Repainting done at texel ("+pixelUV.x+","+pixelUV.y+")."); } }

	private void fireSplash() {
		Ray ray=new Ray(transform.position,transform.forward);
		RaycastHit hitInfo;
		if(Physics.Raycast(ray,out hitInfo,1000f,layerMask)) {
			Transform hitIndicatorTransform=((GameObject)Instantiate(Resources.Load<GameObject>("SplashDecal"))).GetComponent<Transform>(); 
			hitIndicatorTransform.position=hitInfo.point;
			hitIndicatorTransform.LookAt(ray.origin);
			hitIndicatorTransform.Rotate(-90,180,0,Space.Self);
			hitIndicatorTransform=((GameObject)Instantiate(Resources.Load<GameObject>("SplashDecalReversed"))).GetComponent<Transform>(); 
			hitIndicatorTransform.position=hitInfo.point;
			hitIndicatorTransform.LookAt(ray.origin);
			hitIndicatorTransform.Rotate(-90,180,180,Space.Self);
			splashParticle.transform.position=hitInfo.point;
			splashParticle.transform.Translate(-transform.forward*0.5f,Space.World);
			activateSplashParticle(); } }

	private void activateSplashParticle() {
		ParticleSystem[] splashEffect=splashParticle.GetComponentsInChildren<ParticleSystem>();
		for(int a=0; a<splashEffect.Length; a++) splashEffect[a].Play(); }
	
}