﻿using UnityEngine;
using System.Collections;

public class splashParticle : MonoBehaviour {
	public Color splashParticleAndDecalColor=Color.red;
	public Material splashDecalMaterial;
	public Material splashDecalReversedMaterial;

	void Awake() {
		splashDecalMaterial.color=splashParticleAndDecalColor;
		splashDecalReversedMaterial.color=splashParticleAndDecalColor; }

	void Start() {
		ParticleSystem[] splashEffect=GetComponentsInChildren<ParticleSystem>();
		for(int a=0; a<splashEffect.Length; a++) splashEffect[a].startColor=splashParticleAndDecalColor; }

}