using UnityEngine;
using System.Collections;

public class MainCharacterSimpleController : MonoBehaviour {
	public int playerHealth=100;
	private float initialPlayerHealth;
	public int stumbleDamage=30;	//l'ammontare di vita che perde il gelato ogni volta che inciampa
	public int lateralCrashDamage=30;	////l'ammontare di vita che perde il gelato ogni volta che si schianta lateralmente su di un ostacolo causa schivata
	public int dodgeLength=1;	//spostamento laterale in metri della schivata
	public int lateralBoundary=1;	//indica la dimensione in metri massima e minima entro la quale il personaggio potr� muoversi lateralmente, DEVE ESSERE UN MULTIPLO DI "dodgeLength"
	public int jumpHeigth=2;	//altezza in metri del salto
	public int slideDuration=3;	//durata della scivolata prolungata. Tale parametro influisce direttamente sull'animazione ed � quindi invariante dal tempo causa variazioni di framerate ecc.
	public float globalAnimationSpeed=1;	//se >1 la quantit� in eccesso (globalAnimationSpeed-1) andr� sommata alla velocit� (di base le animazioni hanno velocit� 1) di ogni animazione in percentuale al moltiplicatore di velocit� (i parametri sotto) di ciascuna animazione
	public float runAnimationSpeedMultyplier=1;
	public float jumpAnimationSpeedMultyplier=1;
	public float dodgeAnimationSpeedMultyplier=1;
	public float slideAnimationSpeedMultyplier=1;
	public float stumbleAnimationSpeedMultyplier=1;
	public float dodgeCrashAnimationSpeedMultyplier=1;
	public float deathAnimationSpeedMultyplier=1;
	public LayerMask normalObstaclesMask;	//indica con quali layer verificare le collisioni raycast. Deve essere messo su "NormalObstacles" in quanto ci serve per verificare se durante il salto il personaggio atterra su un ostacolo (non da inciampo) e quindi dovr� riprendere a correre
	public LayerMask pavimentMask;	//ci serve insieme alla maschera soprastante per indicare che durante la caduta libera il personaggio dovr� collidere solo con il pavimento e gli ostacoli normali, compenetrer� quindi gli ostacoli da inciampo
	public Transform pavimentTransform;	//necessario a muovere il pavimento
	public int pavimentVelocity=1;	//indica la velocit� (senza unit� di misura) di movimento del pavimento verso il giocatore
	public Transform armatureRotlocController;	//transform dell'oggetto armatureRotolocationController che serve per controllare la posizione e la rotazione del personaggio senza influenzare la posizione/rotazione del collider applicato ad esso
	public GameObject splashParticleGameObj;
	private Vector3 originalSplashParticleGlobalPosition;	//dato che durante la morte causa inciampo lo splashParticle viene spostato nella posizione dell'headbone, tale variabile ci serve per portarlo alla posizione iniziale in caso il giocatore volesse continuare la partita
	public meltingParticle meltingParticleScript;
	public GameObject iceCreamMeshGameObj;
	public Transform headboneTransform;
	public float splashParticlesSize=1;
	public float meltingParticlesSize=1;
	public float decalSize=1;
	public bool stumbleAndDie=true;	//indica se nella morte da inciampo viene riprodotto prima lo stumble e poi la morte da inciampo (=true) o immediatamente la morte da inciampo (=false)
	private Animator animator;
	private AnimatorStateInfo animatorStateInfo;
	private new Collider collider;	//indica il collider associato a questo GameObject
	private RaycastHit hitInfo;
	private float initialPositionX=0;	//indica la posizione laterale iniziale del personaggio. Tale variabile � necessaria per calcolare correttamente gli spostamenti laterali
	private float dodgeBeforeStumbleDestinationX=100;	//indica la posizione finale X del gelato durante la schivata sx/dx prima di un inciampo, in modo da continuare correttamente lo spostamento laterale anche durante l'inciampo, viene usata anche se si inciampa durante un dodgeCrash
	private float scartoIniziale;	//variabile che viene usata insieme alle due soprastanti per gestire lo spostamento laterale durante un dodgeCrash o in casi eccezzionali come l'inciampo durante la schivata o l'inciampo durante un dodgeCrash, in tali casi servir� per permettere al personaggio di tornare nella posizione corretta durante l'inciampo
	private int slideCounter;			//variabile necessaria per terminare la scivolata prolungata qualora questa si sia protratta pi� del massimo consentito da "slideDuration"
	private bool terminated=true;		//indica se un'animazione (eccetto la RunningInPlace) ha terminato
	private bool downArrowReleased;		//indica se durante la scivolata prolungata la freccia gi� � stata rilasciata
	private KeyCode nextMove=KeyCode.None;		//buffer di grandezza unitaria che contiene la prossima animazione da eseguire non appena termina la corrente, in quanto il giocatore potrebbe premere i comandi anche prima che la corrente animazione finisca
	private bool stumble=false;	//indica se l'animazione inciampo va attivata
	private bool dodgeCrash=false;	//indica se l'animazione di schianto laterale va attivata
	private bool deathByCrash=false;	//indica se l'animazione di morte causa impatto frontale va attivata
	private bool deathByStumble=false;	//indica se l'animazione di morte causa inciampo va attivata
	private bool deathByDodgeCrash=false;	//indica se l'animazione di morte causa schianto laterale va attivata
	private Collision lastCollision;	//contiene info riguardanti l'ultima collisione avvenuta
	private int lastCollidedObjectInstanceID=0;
	private int lastCollisionFrameNumber=0;

	// Use this for initialization
	void Start () {
		animator=GetComponent<Animator>(); 
		collider=GetComponent<Collider>();
		initialPlayerHealth=playerHealth;
		meltingParticleScript.setParticlesSize(meltingParticlesSize);
		meltingParticleScript.playerHealth=playerHealth/initialPlayerHealth;
		ParticleSystem[] splashEffect=splashParticleGameObj.GetComponentsInChildren<ParticleSystem>();
		for(int a=0; a<splashEffect.Length; a++) splashEffect[a].startSize*=splashParticlesSize;
		originalSplashParticleGlobalPosition=splashParticleGameObj.transform.position; }

	// Update is called once per frame
	void Update () {
		setAnimationsSpeed();
		animatorStateInfo=animator.GetCurrentAnimatorStateInfo(0);
		meltingParticleScript.playerHealth=playerHealth/initialPlayerHealth;
		meltingParticleScript.playerSpeed=pavimentVelocity;
		//setto la rotazione dell'armatureRotolocationController in modo da compensare l'animazione della scivolata che non avviene proprio dritta, durante l'animazione viene eseguita cos� una rotazione fino a 20� lungo Y
		armatureRotlocController.rotation=Quaternion.AngleAxis(20*animator.GetFloat("RotationDuringSlideCurve"),Vector3.up);
		if(animatorStateInfo.IsName("Death1")) {
			if(animatorStateInfo.normalizedTime>=1) terminated=true;
			return; }
		if(animatorStateInfo.IsName("DeathByStumbleAndDodge")) {
				if(animatorStateInfo.normalizedTime>=1) terminated=true;
				return; }
		if(animatorStateInfo.IsName("DeathByStumbleAndDodgeInPlace")) {
			if(animatorStateInfo.normalizedTime>=1) terminated=true;
			return; }
		else if(deathByStumble&&!animatorStateInfo.IsName("StumbleInPlaceForDeath")&&!animatorStateInfo.IsName("DodgeCrashLeftForDeath")&&!animatorStateInfo.IsName("DodgeCrashRightForDeath")) {
			transform.position=new Vector3(transform.position.x,0,transform.position.z);
			terminated=false;
			deathByStumble=false;
			if(stumbleAndDie) { animator.Play("StumbleInPlaceForDeath"); Debug.Log("Stumbling in place for death"); }
			else { animator.Play("DeathByStumbleAndDodge"); Debug.Log("Died by Stumble"); }
			return; }
		else if(deathByDodgeCrash&&!animatorStateInfo.IsName("StumbleInPlaceForDeath")&&!animatorStateInfo.IsName("DodgeCrashLeftForDeath")&&!animatorStateInfo.IsName("DodgeCrashRightForDeath")) {
			transform.position=new Vector3(transform.position.x,0,transform.position.z);
			terminated=false;
			deathByDodgeCrash=false;
			scartoIniziale=initialPositionX-transform.position.x;
			if(lastCollision.contacts[0].normal==Vector3.right)	animator.Play("DodgeCrashLeftForDeath");
			else animator.Play("DodgeCrashRightForDeath");
			Debug.Log("Dodge crashing for death");
			return; }
		if(deathByCrash) {
			transform.position=new Vector3(transform.position.x,0,transform.position.z);
			transform.forward=-lastCollision.contacts[0].normal;
			animator.Play("Death1"); 
			iceCreamMeshGameObj.SetActive(false);
			ParticleSystem[] splashEffect=splashParticleGameObj.GetComponentsInChildren<ParticleSystem>();
			for(int a=0; a<splashEffect.Length; a++) splashEffect[a].Play();
			printDecal();
			/*sistema che azzera la compenetrazione tra il collider del personaggio ed il collider dell'oggetto colliso, a volte non funziona
			//sposta il parquet un po in avanti in modo da azzerare la compenetrazione tra i collider
			if(lastCollision.contacts[0].normal==Vector3.back) { pavimentTransform.Translate(0,0,collider.bounds.center.z+collider.bounds.size.z/2-lastCollision.collider.bounds.center.z+lastCollision.collider.bounds.size.z/2); }
			//sposta il personaggio un po lateralmente in modo da azzerare la compenetrazione tra i collider
			else if(lastCollision.contacts[0].normal==Vector3.left) { transform.Translate(collider.bounds.center.x+collider.bounds.size.x/2-lastCollision.collider.bounds.center.x+lastCollision.collider.bounds.size.x/2,0,0); }
			else if(lastCollision.contacts[0].normal==Vector3.right) { transform.Translate(lastCollision.collider.bounds.center.x+lastCollision.collider.bounds.size.x/2-collider.bounds.center.x+collider.bounds.size.x/2,0,0); }*/
			deathByCrash=false;
			terminated=false;
			Debug.Log("Died by Crash");
			return; }
		pavimentTransform.Translate(-pavimentTransform.forward*pavimentVelocity*Time.deltaTime);
		if(animatorStateInfo.IsName("StumbleInPlace")) {
			if(dodgeBeforeStumbleDestinationX!=100) transform.position=new Vector3(dodgeBeforeStumbleDestinationX-scartoIniziale*animator.GetFloat("StumbleCurve"),transform.position.y,transform.position.z);
			if(animatorStateInfo.normalizedTime>=0.3&&animatorStateInfo.normalizedTime<1) checkNextMove();
			else if(animatorStateInfo.normalizedTime>=1) { initialPositionX=transform.position.x; dodgeBeforeStumbleDestinationX=100; stumble=false; terminated=true; } }
		else if(stumble&&!animatorStateInfo.IsName("StumbleInPlaceForDeath")&&!animatorStateInfo.IsName("DodgeCrashLeftForDeath")&&!animatorStateInfo.IsName("DodgeCrashRightForDeath")) {//Input.GetKeyDown(KeyCode.Space)) { 
			if((animatorStateInfo.IsName("DodgeLeft")&&animatorStateInfo.normalizedTime<1)||nextMove==KeyCode.LeftArrow) { dodgeBeforeStumbleDestinationX=initialPositionX-dodgeLength; scartoIniziale=dodgeBeforeStumbleDestinationX-transform.position.x; }
			else if((animatorStateInfo.IsName("DodgeRight")&&animatorStateInfo.normalizedTime<1)||nextMove==KeyCode.RightArrow) { dodgeBeforeStumbleDestinationX=initialPositionX+dodgeLength; scartoIniziale=dodgeBeforeStumbleDestinationX-transform.position.x; }
			else if((animatorStateInfo.IsName("DodgeCrashLeft")||animatorStateInfo.IsName("DodgeCrashRight"))&&animatorStateInfo.normalizedTime<1) { dodgeBeforeStumbleDestinationX=initialPositionX; scartoIniziale=dodgeBeforeStumbleDestinationX-transform.position.x; dodgeCrash=false; }
			animator.Play("StumbleInPlace",0,0); animatorStateInfo=animator.GetCurrentAnimatorStateInfo(0); terminated=false; nextMove=KeyCode.None; Debug.Log("Stumbling on frame: "+Time.frameCount); }
		//se sono in aria portami in basso simulando la gravit�, se ero in running o in stumble o in un dodgeCrash o in dodgeSx/Dx esegui quelle, altrimenti fai il checkNextMove() e ritorna
		if(transform.position.y>0&&(animatorStateInfo.IsName("RunningInPlace")||animatorStateInfo.IsName("DodgeLeft")||animatorStateInfo.IsName("DodgeRight")||animatorStateInfo.IsName("StumbleInPlace")||animatorStateInfo.IsName("DodgeCrashLeft")||animatorStateInfo.IsName("DodgeCrashRight"))) {
			if(Physics.Raycast(new Ray(new Vector3(collider.bounds.center.x,collider.bounds.center.y,collider.bounds.center.z+collider.bounds.size.z/2),-transform.up),out hitInfo,5f,normalObstaclesMask|pavimentMask)) {
				if(transform.position.y>hitInfo.point.y) {
					if(Physics.Raycast(new Ray(new Vector3(collider.bounds.center.x,collider.bounds.center.y,collider.bounds.center.z-collider.bounds.size.z/2),-transform.up),out hitInfo,5f,normalObstaclesMask|pavimentMask)) {
						if(transform.position.y>hitInfo.point.y) {
							Vector3 newPosition=transform.position;
							newPosition.y-=4*Time.deltaTime;
							if(newPosition.y<0) newPosition.y=0;
							transform.position=newPosition;
							checkNextMove();
							if(/*!animatorStateInfo.IsName("StumbleInPlace")&&*/!animatorStateInfo.IsName("DodgeLeft")&&!animatorStateInfo.IsName("DodgeRight")&&!animatorStateInfo.IsName("DodgeCrashLeft")&&!animatorStateInfo.IsName("DodgeCrashRight")) return; } } } } }
		if(animatorStateInfo.IsName("StumbleInPlaceForDeath")) return;
		if(animatorStateInfo.IsName("DodgeCrashLeftForDeath")||animatorStateInfo.IsName("DodgeCrashRightForDeath")) {
			Vector3 newPositionX=new Vector3(initialPositionX-scartoIniziale*animator.GetFloat("DodgeCrashCurve"),transform.position.y,transform.position.z);
			if((newPositionX-transform.position).normalized==lastCollision.contacts[0].normal) transform.position=newPositionX;
			return;	}
		if(animatorStateInfo.IsName("DodgeCrashLeft")||animatorStateInfo.IsName("DodgeCrashRight")) {
			transform.position=new Vector3(initialPositionX-scartoIniziale*animator.GetFloat("DodgeCrashCurve"),transform.position.y,transform.position.z);
			if(animatorStateInfo.normalizedTime>=0.3&&animatorStateInfo.normalizedTime<1) { if((checkNextMove()||nextMove!=KeyCode.None)&&animatorStateInfo.normalizedTime>=0.75) { dodgeCrash=false; terminated=true; } }
			if(animatorStateInfo.normalizedTime>=1) { dodgeCrash=false; terminated=true; } }
		else if(dodgeCrash) {//Input.GetKeyDown(KeyCode.Space)) {
			//quello sotto serviva nel caso capitava un dodgeCrash con la normale del punto d'impatto nella stessa direzione di uno stumble laterale e permetteva di continuare a spostare il personaggio 
			//lateralmente nella stessa direzione dello stumble, tuttavia a volte capitava che il personaggio rimaneva nella stessa posizione X durante il dodgeCrash anzich� continuare a spostarsi
			//if(animatorStateInfo.IsName("StumbleInPlace")&&dodgeBeforeStumbleDestinationX!=100) { initialPositionX=dodgeBeforeStumbleDestinationX; dodgeBeforeStumbleDestinationX=100; }
			scartoIniziale=initialPositionX-transform.position.x;
			if(lastCollision.contacts[0].normal==Vector3.right)	animator.Play("DodgeCrashLeft");
			else animator.Play("DodgeCrashRight");
			animatorStateInfo=animator.GetCurrentAnimatorStateInfo(0);
			terminated=false; }
		if(animatorStateInfo.IsName("JumpInPlace1")) {
			Vector3 newPosition=transform.position;
			newPosition.y=jumpHeigth*animator.GetFloat("JumpInPlace1Curve");
			transform.position=newPosition;
			if(animatorStateInfo.normalizedTime>=0.5&&animatorStateInfo.normalizedTime<1) checkNextMove();
			else if(animatorStateInfo.normalizedTime>=1) terminated=true;
			if(animatorStateInfo.normalizedTime>=0.32&&Physics.Raycast(new Ray(new Vector3(collider.bounds.center.x,collider.bounds.center.y,collider.bounds.center.z+collider.bounds.size.z/2),-transform.up),out hitInfo,5f,normalObstaclesMask)) { 
				if(transform.position.y<=hitInfo.point.y) {
					newPosition=transform.position;
					newPosition.y=hitInfo.point.y;
					transform.position=newPosition;
					animator.Play("RunningInPlace"); 
					animatorStateInfo=animator.GetCurrentAnimatorStateInfo(0);
					terminated=true; } } }
		else if(animatorStateInfo.IsName("Slide")) {
			if(!downArrowReleased) {
				if(!Input.GetKey(KeyCode.DownArrow)) { animator.SetFloat("slideSpeedMultyplier",1+(globalAnimationSpeed-1)*slideAnimationSpeedMultyplier); downArrowReleased=true; }
				else {
					if(animatorStateInfo.normalizedTime>=0.45)  animator.SetFloat("slideSpeedMultyplier",-0.25f*(1+(globalAnimationSpeed-1)*slideAnimationSpeedMultyplier));
					else if(animator.GetFloat("slideSpeedMultyplier")<0&&animatorStateInfo.normalizedTime<=0.35) { animator.SetFloat("slideSpeedMultyplier",0.25f*(1+(globalAnimationSpeed-1)*slideAnimationSpeedMultyplier)); slideCounter++; }
					if(slideCounter>=slideDuration) { animator.SetFloat("slideSpeedMultyplier",1+(globalAnimationSpeed-1)*slideAnimationSpeedMultyplier); downArrowReleased=true; } } }
			if(animatorStateInfo.normalizedTime>=0.3&&animatorStateInfo.normalizedTime<1) { if((checkNextMove()||nextMove!=KeyCode.None)&&animatorStateInfo.normalizedTime>=0.72) terminated=true; }
			else if(animatorStateInfo.normalizedTime>=1) terminated=true; }
		else if(animatorStateInfo.IsName("DodgeLeft")&&!terminated) {
				Vector3 newPosition=transform.position;
				newPosition.x=initialPositionX-dodgeLength*animator.GetFloat("DodgeCurve");
				transform.position=newPosition;
				if(animatorStateInfo.normalizedTime>=0.1&&animatorStateInfo.normalizedTime<1) checkNextMove();
				else if(animatorStateInfo.normalizedTime>=1) { initialPositionX=transform.position.x; terminated=true; } }
		else if(animatorStateInfo.IsName("DodgeRight")&&!terminated) {
				Vector3 newPosition=transform.position;
				newPosition.x=initialPositionX+dodgeLength*animator.GetFloat("DodgeCurve");
				transform.position=newPosition;
				Debug.Log("New dodgePositionX: "+newPosition+" on frame: "+Time.frameCount);
				if(animatorStateInfo.normalizedTime>=0.1&&animatorStateInfo.normalizedTime<1) checkNextMove();
				else if(animatorStateInfo.normalizedTime>=1) { initialPositionX=transform.position.x; terminated=true; Debug.Log("DodgingRight finished on frame: "+Time.frameCount); } }
		if(terminated) {
			if(nextMove!=KeyCode.None) {
				switch(nextMove) {
				case KeyCode.UpArrow: { if(transform.position.y==0) { animator.Play("JumpInPlace1",0,0); terminated=false; } nextMove=KeyCode.None; break; }
				case KeyCode.DownArrow: { if(transform.position.y==0) { animator.SetFloat("slideSpeedMultyplier",1+(globalAnimationSpeed-1)*slideAnimationSpeedMultyplier); animator.Play("Slide",0,0);  slideCounter=0; terminated=false; downArrowReleased=false; } nextMove=KeyCode.None; break; }
				case KeyCode.LeftArrow: { if(initialPositionX>-lateralBoundary) { animator.Play("DodgeLeft",0,0); dodgeBeforeStumbleDestinationX=100; terminated=false; } nextMove=KeyCode.None; break; }
				case KeyCode.RightArrow: { if(initialPositionX<lateralBoundary) { animator.Play("DodgeRight",0,0); Debug.Log("Dodging right"); dodgeBeforeStumbleDestinationX=100; terminated=false; } nextMove=KeyCode.None; break; } } }
			else {
				if(Input.GetKeyDown(KeyCode.UpArrow)&&transform.position.y==0) { animator.Play("JumpInPlace1",0,0); terminated=false; }
				else if(Input.GetKeyDown(KeyCode.DownArrow)&&transform.position.y==0) { animator.SetFloat("slideSpeedMultyplier",1+(globalAnimationSpeed-1)*slideAnimationSpeedMultyplier); animator.Play("Slide",0,0); slideCounter=0; terminated=false; downArrowReleased=false; }
				else if(Input.GetKeyDown(KeyCode.LeftArrow)) { if(initialPositionX>-lateralBoundary) { animator.Play("DodgeLeft",0,0); dodgeBeforeStumbleDestinationX=100; terminated=false; } }
				else if(Input.GetKeyDown(KeyCode.RightArrow)) { if(initialPositionX<lateralBoundary) { animator.Play("DodgeRight",0,0); Debug.Log("Dodging right"); dodgeBeforeStumbleDestinationX=100; terminated=false; } }
				else if(!animatorStateInfo.IsName("RunningInPlace")) animator.Play("RunningInPlace"); } }
	}

	private bool checkNextMove() {
		if(Input.GetKeyDown(KeyCode.UpArrow)) { nextMove=KeyCode.UpArrow; return true; }
		if(Input.GetKeyDown(KeyCode.DownArrow)) { nextMove=KeyCode.DownArrow; return true; }
		if(Input.GetKeyDown(KeyCode.LeftArrow)) { nextMove=KeyCode.LeftArrow; return true; }
		if(Input.GetKeyDown(KeyCode.RightArrow)) { nextMove=KeyCode.RightArrow; return true; }
		return false; }

	private void OnCollisionEnter(Collision collisionInfo) { Debug.Log(collisionInfo.contacts[0].point.x+","+collisionInfo.contacts[0].point.y+","+collisionInfo.contacts[0].point.z);
		if(deathByCrash) return;
		if(lastCollidedObjectInstanceID==collisionInfo.gameObject.GetInstanceID()&&Time.frameCount-lastCollisionFrameNumber<=60) return;
		else { lastCollidedObjectInstanceID=collisionInfo.gameObject.GetInstanceID(); lastCollisionFrameNumber=Time.frameCount; }
		if(collisionInfo.gameObject.tag.Equals("StumbleObstacle")) { 
			if(playerHealth-stumbleDamage<=0) { playerHealth=0; deathByStumble=true; Debug.Log("DeathByStumble requested"); return; }
			else playerHealth-=stumbleDamage;
			stumble=true; 
			Debug.Log("Stumble requested");
			return; }
		if(collisionInfo.contacts[0].normal==Vector3.up||collisionInfo.contacts[0].normal==Vector3.forward) return;
		//if(collisionInfo.contacts[0].point.y>=collisionInfo.collider.bounds.center.y-0.3f+collisionInfo.collider.bounds.size.y/2) return;
		if(((collisionInfo.contacts[0].normal==Vector3.right&&!animator.GetCurrentAnimatorStateInfo(0).IsName("DodgeRight"))||(collisionInfo.contacts[0].normal==Vector3.left&&!animator.GetCurrentAnimatorStateInfo(0).IsName("DodgeLeft")))&&(!animator.GetCurrentAnimatorStateInfo(0).IsName("StumbleInPlace")||(animator.GetCurrentAnimatorStateInfo(0).IsName("StumbleInPlace")&&dodgeBeforeStumbleDestinationX!=100&&(dodgeBeforeStumbleDestinationX-initialPositionX)/dodgeLength==-collisionInfo.contacts[0].normal.x))) { 
			if(!dodgeCrash&&!deathByDodgeCrash) lastCollision=collisionInfo;
			if(playerHealth-lateralCrashDamage<=0) { playerHealth=0; deathByDodgeCrash=true; Debug.Log("DeathByDodgeCrash requested"); return; }
			else playerHealth-=lateralCrashDamage;
			dodgeCrash=true; 
			Debug.Log("DodgeCrash requested");
			return; }
		if(collisionInfo.contacts[0].normal==Vector3.back) {
			lastCollision=collisionInfo;
			playerHealth=0;
			deathByCrash=true;
			Debug.Log("DeathByCrash requested"); } }

//serve per stampare il decal durante un'animazione di morte quando il personaggio sbatte la testa per terra o su un ostacolo
//il booleano indica se la printDecal() va eseguita per un'animazione di morte da scontro frontale (=false) o morte da inciampo (=true)
	private void printDecal(bool fromStumbleOrDodgeCrash=false) {
		Transform hitIndicatorTransform=((GameObject)Instantiate(Resources.Load<GameObject>("SplashDecal"))).GetComponent<Transform>();
		if(fromStumbleOrDodgeCrash) { 
			hitIndicatorTransform.position=new Vector3(headboneTransform.position.x,0,headboneTransform.position.z);
			hitIndicatorTransform.localScale=new Vector3(hitIndicatorTransform.localScale.x*decalSize*1.2f,80,hitIndicatorTransform.localScale.z*decalSize*1.2f);
			hitIndicatorTransform.Translate(Vector3.up*0.01f-Vector3.up*hitIndicatorTransform.lossyScale.y/2);
			return; }
		RaycastHit hitInfo;
		if(lastCollision.contacts[0].normal==Vector3.back) {
			Ray ray=new Ray(new Vector3(splashParticleGameObj.transform.position.x,splashParticleGameObj.transform.position.y,collider.bounds.center.z-collider.bounds.size.z/2),Vector3.forward);
			if(Physics.Raycast(ray,out hitInfo,dodgeLength,normalObstaclesMask)) hitIndicatorTransform.position=hitInfo.point;
			else hitIndicatorTransform.position=new Vector3(transform.position.x,lastCollision.collider.bounds.center.y+lastCollision.collider.bounds.size.y/2,lastCollision.collider.bounds.center.z-lastCollision.collider.bounds.size.z/2);
			hitIndicatorTransform.localScale=new Vector3(hitIndicatorTransform.localScale.x,lastCollision.collider.bounds.size.z,hitIndicatorTransform.localScale.z);
			hitIndicatorTransform.forward=Vector3.back; }
		else if(lastCollision.contacts[0].normal==Vector3.right) {
			Ray ray=new Ray(new Vector3(headboneTransform.position.x+collider.bounds.size.x/4,headboneTransform.position.y,lastCollision.contacts[0].point.z),Vector3.left);
			if(Physics.Raycast(ray,out hitInfo,dodgeLength,normalObstaclesMask)) hitIndicatorTransform.position=hitInfo.point;
			else hitIndicatorTransform.position=new Vector3(lastCollision.collider.bounds.center.x+lastCollision.collider.bounds.size.x/2,lastCollision.collider.bounds.center.y+lastCollision.collider.bounds.size.y/2,lastCollision.contacts[0].point.z);
			hitIndicatorTransform.localScale=new Vector3(hitIndicatorTransform.localScale.x,lastCollision.collider.bounds.size.x,hitIndicatorTransform.localScale.z);
			hitIndicatorTransform.forward=Vector3.right; }
		else if(lastCollision.contacts[0].normal==Vector3.left) {
			Ray ray=new Ray(new Vector3(headboneTransform.position.x-collider.bounds.size.x/4,headboneTransform.position.y,lastCollision.contacts[0].point.z),Vector3.right);
			if(Physics.Raycast(ray,out hitInfo,dodgeLength,normalObstaclesMask)) hitIndicatorTransform.position=hitInfo.point;
			else hitIndicatorTransform.position=new Vector3(lastCollision.collider.bounds.center.x-lastCollision.collider.bounds.size.x/2,lastCollision.collider.bounds.center.y+lastCollision.collider.bounds.size.y/2,lastCollision.contacts[0].point.z);
			hitIndicatorTransform.localScale=new Vector3(hitIndicatorTransform.localScale.x,lastCollision.collider.bounds.size.x,hitIndicatorTransform.localScale.z);
			hitIndicatorTransform.forward=Vector3.left; }
		hitIndicatorTransform.Rotate(-90,180,0);
		hitIndicatorTransform.Translate(hitIndicatorTransform.forward*0.01f-hitIndicatorTransform.forward*hitIndicatorTransform.lossyScale.y/2);
		hitIndicatorTransform.localScale=new Vector3(hitIndicatorTransform.localScale.x*decalSize,hitIndicatorTransform.localScale.y,hitIndicatorTransform.localScale.z*decalSize); }

	private void setAnimationsSpeed() {
		animator.SetFloat("runSpeedMultyplier",1+(globalAnimationSpeed-1)*runAnimationSpeedMultyplier);
		animator.SetFloat("jumpSpeedMultyplier",1+(globalAnimationSpeed-1)*jumpAnimationSpeedMultyplier);
		animator.SetFloat("dodgeSpeedMultyplier",1+(globalAnimationSpeed-1)*dodgeAnimationSpeedMultyplier);
		//il moltiplicatore dello slide viene settato sempre nella funzione update() prima di avviare l'animazione slide, pertanto qui viene omesso il settaggio perch� sarebbe codice inutile in pi�
		animator.SetFloat("stumbleSpeedMultyplier",1+(globalAnimationSpeed-1)*stumbleAnimationSpeedMultyplier);
		animator.SetFloat("dodgeCrashSpeedMultyplier",1+(globalAnimationSpeed-1)*dodgeCrashAnimationSpeedMultyplier);
		animator.SetFloat("deathSpeedMultyplier",1+(globalAnimationSpeed-1)*deathAnimationSpeedMultyplier); }

	private void DeathByStumbleAndDodgeSplashAndDecalEffect() {
		iceCreamMeshGameObj.SetActive(false);
		splashParticleGameObj.transform.position=headboneTransform.position;
		ParticleSystem[] splashEffect=splashParticleGameObj.GetComponentsInChildren<ParticleSystem>();
		for(int a=0; a<splashEffect.Length; a++) splashEffect[a].Play();
		printDecal(true); }
}
/*	
LIMITAZIONI
-- non posso inciampare durante la discesa in caduta libera da un ostacolo, se cado lateralmente l'inciampo funziona, se cado in avanti non funziona
-- SOLUZIONE: il gioco non dovrebbe per natura contenere un inciampo subito dopo un ostacolo, ma dovrebbe consentire al giocatore di toccare terra prima di proporgli qualsiasi altro tipo di ostacolo
-- se capito su due inciampi di fila l'animazione di inciampo viene riprodotta solo per il primo mentre invece dovrebbe essere riprodotta ogni volta che capito in un ostacolo da inciampo, anche se consecutivi
-- SOLUZIONE: il gioco non dovrebbe produrre due inciampi di fila, ma dovrebbe dare al giocatore la possibilit� di fare una mossa almeno subito dopo il primo inciampo
-- non posso saltare se sono sopra un ostacolo
-- non posso saltare lateralmente, ovvero saltare e nel mentre cambiare corsia
*/