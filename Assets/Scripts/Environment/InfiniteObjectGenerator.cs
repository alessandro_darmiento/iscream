﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp;

/*
 * The InfiniteObjectGenerator is the controlling class of when different objects spawn.
 */
using System;


public class InfiniteObjectGenerator : MonoBehaviour {

	public enum ObjectLocation { Center, Left, Right, Last }

	public static InfiniteObjectGenerator mInstance;
	//public static readonly Vector3 CACHE_LOCATION = new Vector3 (-1000, -1000, -1000);

	//This gameObject must be set as the empty gameObject in which each otherGameObject is child. 
	//All world rotation will be applied to this gameObject
	public GameObject sceneContainer;

	//This gameobject must be set as the collider box in which each platform will be removed from the scene. 
	//All the platform will also be attracted by this point
	public GameObject swallower;

	//Cache gameObject 
	public GameObject cache;

	//This array contains ALSWAY one 1 and two 0. The 1 represent the direction to translate the platforms
	private Vector3 moveDirection; 
	private Vector3 spawnDirection;
	private float wrongMoveDistance;

	private Transform playerTransform;

	//Cache of spawned gameObject not active atm. Enhance performance. The value is a container so it is possible to store more than an obj of each type
	private Dictionary<string, GameObjectContainer> objectCache;

	//List of the active GameObjects in the scene.
	private List<GameObject> livingObjects;

	//This is a subset of livingObjects with all the platforms which have to be translated to the player at each frame.
	private List<GameObject> livingPlatforms;

	//Last spawned platform. Need this to set continuos spawning
	private GameObject lastPlatform; 
	//
	private int pieceToAttach;

	private bool alternativePopulation = false; //This boolean get activated with a custom probability. When activated, a different algorithm is used to populate platform, with moving obstacles
	private int alternativePopulationCounter;
	private int alternativePopulationMaxCounter = 5;
	public float alternativePopulationProbability = 5f; //5% for each platform

	private int totalPlatformsSinceEpoch;

	public void Awake()
	{
		mInstance = this;	
	}

	public void Start()
	{
		Debug.Log ("InfiniteObjectGenerator start");
		mInstance = this;
		totalPlatformsSinceEpoch = 0;
		alternativePopulation = false;
		swallower.transform.parent = sceneContainer.transform;
		moveDirection = Vector3.forward;
		spawnDirection = Vector3.forward;
		objectCache = new Dictionary<string, GameObjectContainer> ();
		livingObjects = new List<GameObject> ();
		livingPlatforms = new List<GameObject> ();
		ShowStartupObjects ();

	}

	public static InfiniteObjectGenerator getInstance(){
		if (null == mInstance) {
			//Come si inizializza sto coso #unitymerda #volevofarejava 
		}
		return mInstance;
	}

	// creates any startup objects
	public bool ShowStartupObjects()
	{
		Debug.Log (Time.time + " ShowStartupObjects");
		GameObject startUpObject = Resources.Load<GameObject> ("Prefab/Platform/startPlatform");
		startUpObject = createObject (startUpObject);
		startUpObject.transform.parent = sceneContainer.transform;
		startUpObject.transform.position = new Vector3 (0, 0, -9);
		lastPlatform = startUpObject;
		livingPlatforms.Add(startUpObject);
		return true;
	}

	private void StartGame()
	{
		//TODO
	}


	public void spawnPlatforms(){
		spawnPlatforms (5);
	}

	public void spawnPlatforms(int count){
		//Debug.Log ("Spawning: " + count + " platforms");
		GameObject platformToSpawn, spawnedPlatform;
		for (int i = 0; i < count; i++) {
			try{
				try{
					platformToSpawn = AppearanceRules.getInstance ().getNextPlatform ();
					spawnedPlatform = createObject (platformToSpawn);
					spawnedPlatform.transform.parent = sceneContainer.transform;
					try{
						//Create sides, attach to lastPlatform and populate with collidables
						stylizePlatform(spawnedPlatform);
					} catch(Exception e){
						Debug.LogError(e.Message);
					}

					try{
						//Make the platform contig with the other ones
						attachPlatform(spawnedPlatform);
					} catch(Exception e){
						Debug.LogError(e.Message);
					}

					try{
						//Spawn objects at random position on the platform
						populatePlatform(spawnedPlatform); 
					} catch(Exception e){
						Debug.LogError(e.Message);
					}
					//Keep the platform in the list of objects to translate
					livingPlatforms.Add(spawnedPlatform);
					//Keep the platform as child of the sceneContainer. All platform are therefore siblings
					//Debug.Log ("Created: " + i + "° platform");
				} catch(Exception e){
					Debug.LogError(e.Message);
				}
			} catch(Exception e){
				Debug.LogError (e.Message);
			}
			totalPlatformsSinceEpoch++;

			if (!alternativePopulation && shouldActivateAlternativePouplation ()) {
				Debug.Log ("Starting populating with moving obstacles!");
				alternativePopulation = true;
				alternativePopulationCounter = 0;
			}

			if (alternativePopulation) {
				alternativePopulationCounter++;
				if (alternativePopulationCounter > alternativePopulationMaxCounter) {
					alternativePopulation = false;
				}
			}

		}
	}

	private bool shouldActivateAlternativePouplation (){
		if (totalPlatformsSinceEpoch > 20) {//TODO: tweak this and use GLOBAL CONSTANT
			if(UnityEngine.Random.Range(0, 100) < alternativePopulationProbability){
				return true;
			}
		}
		return false;
	}


	/**
	 * Remove from the scene a Platform gameobject and all collidable children
	 * Each child is detached from the parent before destruction in order to keep it available for cache 
	 */
	public void removePlatform(GameObject platform){
		//Debug.Log ("Removing: " + gameObject.name + " platform from scene");
		platform.GetComponent<PlatformObject>().normalize();
		livingPlatforms.Remove (platform);
		destroyObject (platform);
	}

	//Create platform sides and other style stuff
	public void stylizePlatform(GameObject platform){
		//Debug.Log ("StylizePlatform");
		GameObject sideObject;
		Transform attachPoint;
		//Left side
		try{
			//fetch
			sideObject = createObject (AppearanceRules.getInstance ().getNextSide());
			attachPoint = platform.GetComponent<PlatformObject>().getLeftPointer();
			//position
			sideObject.transform.parent = attachPoint;
			sideObject.transform.position = attachPoint.position;
			//rotation
			sideObject.transform.localRotation = new Quaternion(0, 0, -1, 1);
			platform.GetComponent<PlatformObject>().addChild(sideObject);
		}
		catch(Exception e){
			Debug.LogError (e.Message);
		}
		//Right side
		try{
			//fetch
			sideObject = createObject (AppearanceRules.getInstance ().getNextSide());
			attachPoint = platform.GetComponent<PlatformObject>().getRightPointer();
			//position
			sideObject.transform.parent = attachPoint;
			sideObject.transform.position = attachPoint.position;
			//rotaton
			sideObject.transform.localRotation = new Quaternion(0, 0, 1, 1);
			platform.GetComponent<PlatformObject>().addChild(sideObject);
		} catch(Exception e){
			Debug.LogError (e.Message);
		}
		//TODO: add other style components
	}

	/**
	 * Set the position of the given platform platform as a contig of the lastPlaftom object
	 * Then override the lastPlatform obj with the newly spawned platfomr
	 **/
	public void attachPlatform(GameObject platform){
		//platform.transform.position = lastPlatform.transform.GetChild(MESH_POINTER).GetChild(END_POINTER).position;
		platform.transform.position = lastPlatform.GetComponent<PlatformObject>().getEndPointer().position;
		platform.transform.rotation = new Quaternion(0, 0, 0, 1);
		lastPlatform = platform;
		//Debug.Log ("New lastPlatform: " + gameObject.name);
	}

	//Try to fit as many collidable objects in the plaform to the max collidable slots in the platform
	/**
	 * Populate the platform with as many collidable object as possible until maxCollidable is reached.
	 * Compute a random based queue of preference for lane to have the collidable positioned referring to platform costraints
	 * For each preference layer tries to find a collision-free position for the new collidable object to spawn
	 * If it is not possible to find a feasible position in the given lane after multiple fails, the lane is marked as full and 
	 * no more collidable will be placed here.
	 * If is not possible to find a feasible position in any lane, then the platform itself is considered full and no more 
	 * collidable placed in it
	 */ 
	public void populatePlatform(GameObject platform){
			
		if (alternativePopulation) {
			//Doesn't populate the first platform to avoid funny overlap with moving obstacles
			if (alternativePopulationCounter < 2)
				return;
		}

//		Debug.Log ("PopulatePlatform");
		GameObject collidable;//=null;
		Vector3 position; //= new Vector3(-1, -1, -1) ;
		ArrayList lanesQueue;
		int laneIndex, pickingAttempts;
		bool success=false, criticalFail=false, pickSuccess;
		int slot = platform.GetComponent<PlatformObject> ().getCollidableSlots ();
		//Debug.Log ("CollidableSlot: " + slot);

		try{

			if(alternativePopulation){
				slot = 2; //TODO: fixed? 
			}

			for(int i = 0; i < slot && !criticalFail; i++){
				//spawn
				pickingAttempts = 0;
				pickSuccess = false;
				do{
					if(alternativePopulation){
						collidable = AppearanceRules.getInstance ().getNextMovingObstacle (); 
					} else{
						collidable = AppearanceRules.getInstance ().getNextCollidable ();
					}
					pickSuccess = platform.GetComponent<PlatformObject>().supportThisCollidable(collidable);
				} while(pickingAttempts<3 && !pickSuccess);
					
				if(pickSuccess){
					
				}

				collidable = createObject (collidable);

				success = false;
				laneIndex = 0;

				lanesQueue = suggestLanes(platform, collidable);

				while(!success && lanesQueue!=null && lanesQueue.Count > 0 ){
					laneIndex = (int)lanesQueue[0];
					collidable.transform.parent = platform.transform;
					position = findCollidablePosition(platform, collidable, laneIndex);
					//try{
						if(new Vector3(-1, -1, -1) == position){
							//Failure
							Debug.Log("Cannot find suitable position for collidable: " + collidable.name + " inside platform " + platform.name + ". Checking next lane if present");
						} else {
							success = true;
							collidable.transform.parent = platform.transform;
							collidable.transform.position = position;
							collidable.GetComponent<CollidableObject>().setLane(laneIndex);
							platform.GetComponent<PlatformObject>().setLastCollidable(collidable, laneIndex);
							platform.GetComponent<PlatformObject>().addChild(collidable);
						}
				}
				if (!success){ //If I weren't able to find any feasible position in any lane, it's pointless to try to fit other collidables
					criticalFail = true;	
					destroyObject(collidable);
				} 
							
			}
		}catch(Exception e){
			Debug.LogError (e.Message);
		}
	}
		


	/*
	 * This methods computes a collision free position for the collidable. It is also checked that every hard costraint is respected 
	 * Hard costrainsts: no overlap, always inside platform, always at least a safe passage
	 */
	private Vector3 findCollidablePosition(GameObject platform, GameObject collidable, int laneIndex){
		Vector3 position = new Vector3();
		Vector2 lanePosition = platform.GetComponent<PlatformObject> ().getLaneXY (laneIndex);
		GameObject lastCollidable = platform.GetComponent<PlatformObject> ().getLastCollidable (laneIndex);
		float INNER_DISTANCE = 1; //TODO: THIS IS A GLOBAL VARIABLE WHICH DEPENDS ON SPEED
		int MAX_ATTEMPTS = 3; //TODO: THIS IS A GLOBAL CONSTANT
		float zValue;
		bool success = false;
		int attempts = 0;

		if (null == lastCollidable) { // First collidable in this lane
			zValue = platform.transform.position.z + platform.GetComponent<PlatformObject> ().getMinZOffset();

		} else { 
			//The Z position will be the minimum position after the collider position + a minimum inner distance
			zValue = lastCollidable.transform.position.z + lastCollidable.GetComponent<CollidableObject>().getMaxBounds().size.z + INNER_DISTANCE;
		}
	

		//Now check if an hard costraint is violated. 
		//Add random value to z and try again
		while (!success && attempts < MAX_ATTEMPTS) {
			attempts++;
			position = new Vector3(lanePosition.x, lanePosition.y, zValue);
			if (platform.GetComponent<PlatformObject>().doesViolateCostraints(position, collidable, laneIndex)){
				//zValue is incremented by a random value from another INNER_DISTANCE to the max distance between current zValue and the end of the platform, with respect of the collidable size
				zValue += UnityEngine.Random.Range (INNER_DISTANCE, (platform.GetComponent<PlatformObject>().getMaxBounds().max.z - zValue - collidable.GetComponent<PlatformObject>().getMaxBounds().size.z));
			} else {
				success = true;
			}
		}

		if(!success)
			position = new Vector3(-1, -1, -1);

		Debug.Log ("Computed position for " + collidable.name + ": " + position + ". lastCollidable was null: " + (lastCollidable == null));
		return position;
	}


	/*
     * This methods computes a collision free position for the movingObstacle. 
	 */ 
	private Vector3 findAlternativePopulationPosition(GameObject platform, GameObject collidable, int laneIndex){
		//TODO
		return Vector3.up;
	}


	/**
	 * Based on platform constraints, compute a queue of preferred lanes from most liked one to least liked one.
	 * The queue is implemented through a List to simplify implementation
	 */ 
	private ArrayList suggestLanes(GameObject platform, GameObject collidable){
		//Debug.Log ("suggestLanes");
		try{
			bool[] platformLanesEnabled = platform.GetComponent<PlatformObject> ().getCollidableLanesEnabled ();
			bool[] obstacleLanesEnabled = collidable.GetComponent<CollidableObject> ().getCollidableLanesEnabled ();
			ArrayList preferredLanes = new ArrayList();
			for (int i = 0; i < platformLanesEnabled.Length; i++) {
				if (platformLanesEnabled [i] && obstacleLanesEnabled[i]) {
					if (UnityEngine.Random.Range (0, 100) > 50) {
						preferredLanes.Insert (0, i);
					} else {
						preferredLanes.Add (i);
					}
				}
			}

			/*
			for(int k = 0; k< preferredLanes.Count; k++){
				Debug.Log (Time.time + "Platform " + platform.name +" lanes  " + k + ": " + (int)preferredLanes[k]);
			}
			*/
			return preferredLanes;
		}
		catch(Exception e){
			Debug.LogError (e.Message);
		}
		return null;
	}

	/**
	 * Instantiate or get from cache a GameObject
	 * @param gameObject is the non yet spawned gameObject
	 * returns the pointer to the instantiated gameObject
	*/
	public GameObject createObject(GameObject gameObject){
		//Debug.Log ("Creating: " + gameObject.name + " gameObject");
		GameObject output;
		string key = gameObject.name.Replace ("(Clone)", "");
		if (objectCache.ContainsKey (key) && objectCache [key].getObjectList ().Count > 0) {
			output = objectCache [key].getObject ();
			output.SetActive (true);
			//Debug.Log (Time.time + " Recovered from pool " + key);
		} else {
			output = Instantiate (gameObject);
			//Debug.Log (Time.time + " Spawned " + key);
		}	
		livingObjects.Add (output);
	
		return output;
	}

	/* 
	 * Destroy or cache GameObject
	 * @oaram gameObject is the instantiated gameObject to remove
	 */ 
	public void destroyObject(GameObject gameObject){
		//Debug.Log ("Destroying: " + gameObject.name + " gameObject");
		string key = gameObject.name.Replace ("(Clone)", "");
		livingObjects.Remove (gameObject);
		if (objectCache.ContainsKey (key)) {
			if (objectCache [key].getObjectList ().Count >= GameObjectContainer.CACHE_LIMIT) {
				//Debug.Log (Time.time + " " + key + " already cached above the limit. Destroying");
				Destroy (gameObject);
			} else {
				objectCache [key].pushGameObject (gameObject);
				gameObject.transform.position = cache.transform.position;
				gameObject.transform.parent = cache.transform;
				//gameObject.transform.Translate (CACHE_LOCATION);
				gameObject.SetActive (false);
				//Debug.Log (Time.time + " " + key + " cached within it's container");
			}
		} else {
			objectCache.Add (key, new GameObjectContainer (gameObject));
			gameObject.transform.position = cache.transform.position;
			gameObject.transform.parent = cache.transform;
			//gameObject.transform.Translate (CACHE_LOCATION);
			gameObject.SetActive (false);
			//Debug.Log (Time.time + " New container created for " + key);
		}
	}


	public void printCacheContent(){
		Debug.Log (Time.time + " Printing cache container: " + objectCache.Count + " key-value pair");
		foreach (KeyValuePair<string, GameObjectContainer> entry in objectCache) {
			Debug.Log ("\n" + Time.time + " key: " + entry.Key + ", value count: " + entry.Value.getObjectList().Count);
		}
	}


	//Debug stuff	
	//int i = 0;
	//int j = -400;
	//int k = 0;
	// Update is called once per frame
	void Update () {
		//playerTransform = MainCharacterSimpleController.getInstance ().gameObject.transform;
		if (livingPlatforms.Count < 10) {
			spawnPlatforms (10 - livingPlatforms.Count);
		}

		//Should the be performed by an external controller?
		translatePlatforms();
	}
		
	/**
	 * Translate the platforms towards the player
	 */ 
	private void translatePlatforms(){
		//Debug.Log ("translatePlatform. livingPlatforms.Count: " + livingPlatforms.Count);
		//Debug.Log ("Target position: " + swallower.transform.position);
		//TODO: speed should be read from the globalVariables container
		float speed = 1f;
		foreach (GameObject platform in livingPlatforms) { //Shoud this be a unique gameObject containing all the active platforms? 
			platform.transform.Translate(0, 0, swallower.transform.position.z*Time.deltaTime*speed);
		}
	}



}
