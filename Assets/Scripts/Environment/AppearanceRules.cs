﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AppearanceRules : MonoBehaviour {

	public enum WorldType {Seashore, Town, todo}
	public enum CollidableType {Coin, Obstacle, PowerUp, Crystal}
	public static AppearanceRules mInstance;

	private WorldType currentWorld;

	//Must sum up to 100;
	public float powerUpProbability = 20;
	public float obstacleProbability = 40;
	public float coinProbability = 30;
	public float crystalProbability = 10;

	public readonly int minSectionsBeforeWorldChange = 50;
	public readonly int maxSectionsBeforeWorldChange = 100;
	public readonly float worldChangeProbability = 5;

	//Refill the queue when there are less than this number of elements
	private static int refillCount = 10;
	public Queue<GameObject> platformQueue;
	public Queue<GameObject> collidableQueue;
	public Queue<GameObject> sidesQueue;
	public Queue<GameObject> movingObstaclesQueue;

	private int currentWorldSectionsCount;
	//Arrays of all Spawnable prefabs
	private GameObject[] powerUps;
	private GameObject[] coins;
	private GameObject[] crystals;
	private GameObject[] townObstacles;
	private GameObject[] seashoreObstacles;
	private GameObject[] townPlatforms;
	private GameObject[] seashorePlatforms;
	private GameObject[] commonObstacles;
	private GameObject[] commonPlatforms;
	private GameObject[] seashoreSides;
	private GameObject[] townSides;
	private GameObject[] commonSides;
	private GameObject[] movingObstacles;

	public WorldType getCurrentWorldType(){
		return currentWorld;
	}

	//Get a collidableObject
	public CollidableType getNextCollidableType(){
		if (Mathf.Round(powerUpProbability + obstacleProbability + coinProbability + crystalProbability) != 100) {
			liftProbabilities ();
		}
		int range = UnityEngine.Random.Range (0, 100);
		if (range <= powerUpProbability) {
			return CollidableType.PowerUp;
		}
		if (range <= powerUpProbability + obstacleProbability) {
			return CollidableType.Obstacle;
		}
		if (range <= powerUpProbability + obstacleProbability + coinProbability) {
			return CollidableType.Coin;
		}
		return CollidableType.Crystal;
	}

	private void liftProbabilities(){
		float sum = powerUpProbability + obstacleProbability + coinProbability + crystalProbability;
		powerUpProbability = (powerUpProbability * 100) / sum;
		obstacleProbability = (obstacleProbability * 100)/sum;
		coinProbability = (coinProbability * 100)/sum;
		crystalProbability = (crystalProbability * 100)/sum;
	}

	//Get a platform platform to spawn
	public GameObject getNextPlatform(){
		//Debug.Log ("getNextPlatform. platformQueue.Count: " + platformQueue.Count);
		while (platformQueue.Count < refillCount) {
			refillPlaftormQueue (refillCount);
		}
		currentWorldSectionsCount++;
		return platformQueue.Dequeue ();
	}

	//Keep the platform queue full
	private void refillPlaftormQueue(int count){
		//Debug.Log  ("refillQueue by " + count + " items");
		//Debug.Log  ("queue old size: " + platformQueue.Count);
		for (int i = 0; i < count; i++) {
			switch (currentWorld){
			case WorldType.Seashore:
				platformQueue.Enqueue (seashorePlatforms [UnityEngine.Random.Range (0, seashorePlatforms.Length - 1)]);
				sidesQueue.Enqueue (seashoreSides [UnityEngine.Random.Range (0, seashoreSides.Length - 1)]);
				sidesQueue.Enqueue (seashoreSides [UnityEngine.Random.Range (0, seashoreSides.Length - 1)]);
				break;
			case WorldType.Town:
				platformQueue.Enqueue (townPlatforms [UnityEngine.Random.Range (0, townPlatforms.Length - 1)]);
				sidesQueue.Enqueue (townSides [UnityEngine.Random.Range (0, townSides.Length - 1)]);
				sidesQueue.Enqueue (townSides [UnityEngine.Random.Range (0, townSides.Length - 1)]);
				break;
			}

			if(shouldChangeWorld()){
				try{
					Debug.Log  ("Changing world. Section count: " + currentWorldSectionsCount);
					changeWorld ();
					return; //Prevent further iterations
				} catch(Exception e){
					Debug.LogError (e);
				}
			}
			//currentWorldSectionsCount++;
		}
		//Debug.Log  ("queue new size: " + platformQueue.Count);
	}

	private bool shouldChangeWorld(){
		if (currentWorldSectionsCount < minSectionsBeforeWorldChange)
			return false;
		if (currentWorldSectionsCount >= maxSectionsBeforeWorldChange)
			return true;
		if(UnityEngine.Random.Range (0, 100) < worldChangeProbability)
			return true;
		return false;
	}

	/**
	 * Reseat each queue then enqueue an asset with common piece, then refill with new world pieces.
	 * Slow and unefficient as fuck, but this should be run few times.
	 */ 
	private void changeWorld(){
		int COMMON_PLACE_NUMBER = 2; //TODO

		//Clear current data
		platformQueue.Clear ();
		collidableQueue.Clear ();
		sidesQueue.Clear ();

		//Enqueue common set
		for (int i = 0; i < COMMON_PLACE_NUMBER; i++) {
			platformQueue.Enqueue (commonPlatforms [UnityEngine.Random.Range (0, commonPlatforms.Length - 1)]);
			sidesQueue.Enqueue (commonSides [UnityEngine.Random.Range (0, commonSides.Length - 1)]);
			sidesQueue.Enqueue (commonSides [UnityEngine.Random.Range (0, commonSides.Length - 1)]);
		}
		currentWorldSectionsCount = 0;

		//TODO: fare meglio
		if(currentWorld.Equals(WorldType.Seashore)){
			currentWorld = WorldType.Town;
		}else{
			currentWorld = WorldType.Seashore;
		}
	}


	public GameObject getNextCollidable(){
		if (collidableQueue.Count < refillCount) {
			refillCollidableQueue (refillCount);
		}
		return collidableQueue.Dequeue ();
	}

	public GameObject getNextMovingObstacle(){
		if (movingObstaclesQueue.Count < refillCount) {
			refillMovingObstaclesQueue (refillCount);
		}
		return movingObstaclesQueue.Dequeue ();
	}

	public GameObject getNextSide(){
		/*while (sidesQueue.Count < refillCount) { gestito da platform
			refillSidesQueue (refillCount);
		}	*/
		return sidesQueue.Dequeue ();
	}

	//TODO: optimize
	private void refillCollidableQueue(int refillCount){
		CollidableType type;
		for (int i = 0; i < refillCount; i++) {
			type = getNextCollidableType ();
			switch (type) {
			case CollidableType.Coin:
				collidableQueue.Enqueue (coins [UnityEngine.Random.Range (0, coins.Length - 1)]);
				break;
			case CollidableType.Crystal:
				collidableQueue.Enqueue (crystals [UnityEngine.Random.Range (0, crystals.Length - 1)]);
				break;
			case CollidableType.Obstacle:
				if (UnityEngine.Random.Range (0, 100) > 40) { //TODO: tweak params
					collidableQueue.Enqueue (commonObstacles [UnityEngine.Random.Range (0, commonObstacles.Length - 1)]);
				} else {
					switch (currentWorld) {
					case WorldType.Seashore:
						collidableQueue.Enqueue (seashoreObstacles [UnityEngine.Random.Range (0, seashoreObstacles.Length - 1)]);
						break;
					case WorldType.Town:
						collidableQueue.Enqueue (townObstacles [UnityEngine.Random.Range (0, townObstacles.Length - 1)]);
						break;
					}
				}
				break;
			case CollidableType.PowerUp:
				collidableQueue.Enqueue (powerUps [UnityEngine.Random.Range (0, coins.Length - 1)]);
				break;
			}
		}
	}

	private void refillMovingObstaclesQueue(int refillCount){
		for (int i = 0; i < refillCount; i++) {
			movingObstaclesQueue.Enqueue (movingObstacles [UnityEngine.Random.Range (0, movingObstacles.Length - 1)]);
		}
	}

	//TODO: optimize this
	private void resetCollidableQueue(){
		collidableQueue.Clear ();
		refillCollidableQueue (refillCount);
	}


	// Use this for initialization
	void Start () {
		print ("AppearanceRules start");
		mInstance = this;
		currentWorldSectionsCount = 0;
		currentWorld = WorldType.Seashore;
		platformQueue = new Queue<GameObject> ();
		collidableQueue = new Queue<GameObject> ();
		sidesQueue = new Queue<GameObject> ();
		movingObstaclesQueue = new Queue<GameObject> ();


		//Load prefab list
		seashorePlatforms = Resources.LoadAll<GameObject>("Prefab/Platform/Seashore");
		Debug.Log ("SeashorePlatform size: " + seashorePlatforms.Length);
		townPlatforms = Resources.LoadAll<GameObject> ("Prefab/Platform/Town");
		//Debug.Log  ("townPlatform size: " + townPlatforms.Length);
		powerUps = Resources.LoadAll<GameObject>("Prefab/Collidable/PowerUp");
		coins = Resources.LoadAll <GameObject>("Prefab/Collidable/Coins");
		crystals = Resources.LoadAll<GameObject>("Prefab/Collidable/Crystal");
		commonObstacles = Resources.LoadAll<GameObject>("Prefab/Collidable/Obstacle/Common");
		townObstacles = Resources.LoadAll<GameObject> ("Prefab/Collidable/Obstacle/Town"); 
		seashoreObstacles = Resources.LoadAll<GameObject>("Prefab/Collidable/Obstacle/Seashore");
		commonPlatforms = Resources.LoadAll<GameObject>("Prefab/Platform/Common");
		//Debug.Log  ("CommonPlatform size: " + commonPlatforms.Length);
		seashoreSides =  Resources.LoadAll<GameObject>("Prefab/Sides/Seashore");
		townSides =  Resources.LoadAll<GameObject>("Prefab/Sides/Town");
		commonSides =  Resources.LoadAll<GameObject>("Prefab/Sides/Common");
		//Debug.Log  ("common sides size: " + commonSides.Length);
		//Debug.Log  ("sea sides size: " + seashoreSides.Length);
		//Debug.Log  ("town sides size: " + townSides.Length);
		movingObstacles = Resources.LoadAll<GameObject>("Prefab/Collidable/MovingObstacles");

		//Start with some obj
		refillPlaftormQueue (10); 
		refillCollidableQueue (10);
		refillMovingObstaclesQueue (10);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static AppearanceRules getInstance(){
		return mInstance;
	}
		
}
