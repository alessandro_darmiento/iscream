﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlatformObject : InfiniteObject {

	public readonly static ObjectType OBJECT_TYPE = ObjectType.Platform;

	//Wasting some bits because elseware I'd have too much great performance and it wouls be mean to other groups
	public readonly static int LEFT_LANE = 0;
	public readonly static int MID_LANE = 1;
	public readonly static int RIGHT_LANE = 2;
	public readonly static int[] LANES = {LEFT_LANE, MID_LANE, RIGHT_LANE};

	public readonly int MESH_POINTER = 0;
	public readonly int END_POINTER = 0;
	public readonly int LEFT_POINTER = 1;
	public readonly int RIGHT_POINTER = 2;
	public readonly int LEFT_LANE_POINTER = 3;
	public readonly int MID_LANE_POINTER = 4;
	public readonly int RIGHT_LANE_POINTER = 5;

	public float minZOffset = 0; //Minimum Z for collidable spawning


	private Transform leftPointer, rightPointer, endPointer, leftLanePointer, midLanePointer, rightLanePointer; //TODO use this instead of re-compute every time
	private Bounds bound;
	private GameObject[] lastCollidable;


	//TODO: Include slopes?
	public enum PlatformSlope { None, Up, Down }

	// Direction of platform. At least one option must be true. Straight is the most common so is the default
	public bool straight = true;
	public bool leftTurn;
	public bool rightTurn;

	// the number of collidable objects that can fit on one platform. The objects are spaced along the local z position of the platform
	public int collidableSlots;

	// boolean to determine what horizontal location objects can spawn. If collidablePositions is greater than 0 then at least one
	// of these booleans must be true
	public bool collidableLeftSpawn;
	public bool collidableCenterSpawn;
	public bool collidableRightSpawn;

	//List of all children of this platform
	private List<GameObject> children; 

	private float swallowPosition; 

	public override void Init()
	{
		base.Init();
		Debug.Log ("Spawned " + gameObject.name);
		children = new List<GameObject>();
	}
		

	public override void Awake()
	{
		//Debug.Log (Time.time + " " + this.gameObject.name + " Awake()");
		base.Awake();
		objectType = ObjectType.Platform;
		children = new List<GameObject>();
		CollidableObject[] collidableObjects = GetComponentsInChildren<CollidableObject>();
		lastCollidable = new GameObject[3];
		for (int i = 0; i < collidableObjects.Length; ++i) {
			collidableObjects[i].SetStartParent(collidableObjects[i].transform.parent);
		}

		// If this platfom doesn't have any direction enabled then it won't do anything
		if (!straight && !leftTurn && !rightTurn) {
			Debug.LogWarning(thisGameObject.name + " has no direction set.");
			straight = true;
		}

		for (int i = 0; i < transform.GetChild(0).childCount; i++) {
			if (transform.GetChild(0).GetChild (i).gameObject.CompareTag ("Pointer")) {
				transform.GetChild(0).GetChild (i).gameObject.GetComponent<MeshRenderer>().enabled = false;
			}
		}

		try{
			leftPointer = gameObject.transform.GetChild(MESH_POINTER).GetChild(LEFT_POINTER);
			rightPointer = gameObject.transform.GetChild(MESH_POINTER).GetChild(RIGHT_POINTER);
			endPointer = gameObject.transform.GetChild(MESH_POINTER).GetChild(END_POINTER);
			leftLanePointer = gameObject.transform.GetChild(MESH_POINTER).GetChild(LEFT_LANE_POINTER);
			rightLanePointer = gameObject.transform.GetChild(MESH_POINTER).GetChild(RIGHT_LANE_POINTER);
			midLanePointer = gameObject.transform.GetChild(MESH_POINTER).GetChild(MID_LANE_POINTER);
			//TODO: lanesPointer
		} catch(Exception e){
			Debug.LogError (e.Message);
		}finally{
			//Debug.Log ("EndPointer: " + endPointer.position);
			//Debug.Log ("leftPointer: " + leftPointer.position);
			//Debug.Log ("rightPointer: " + rightPointer.position);
		}

		//beginLocation = gameObject.transform.
		swallowPosition = staticCameraReference.getInstance().gameObject.transform.position.z -20;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.transform.position.z < swallowPosition) {
			InfiniteObjectGenerator.getInstance ().removePlatform (this.gameObject);
		}
	}

	public int getCollidableSlots(){
		return collidableSlots;
	}

	/**
	 * Add a generic object (such as collidable one) to a list for easier caching normalization
	 */ 
	public void addChild(GameObject gameObject){
	//	Debug.Log ("addChild: " + gameObject.name);
		children.Add (gameObject);
	}

	public void removeChild(GameObject gameObject){
		if (children.Contains (gameObject)) {
			children.Remove (gameObject);
		}
	}

	public void clearChildren(){
		children.Clear ();
	}

	public List<GameObject> getCollidableChildren(){
	//	Debug.Log ("getCollidableChildren: " + collidableChildren.Count);
		return children;
	}
		
		
	/**
	 * Get in which lane a collidable can be spawn
	 */ 
	public bool[] getCollidableLanesEnabled(){
		//bool[] b = new bool[3]{ collidableLeftSpawn, collidableCenterSpawn, collidableRightSpawn };
		//Debug.Log ("getCollidableLanesEnabled: " + b[0] + " " + b[1] + " " + b[2]);
		//return b;
		return new bool[3]{collidableLeftSpawn, collidableCenterSpawn, collidableRightSpawn};
	}

	public Transform getLeftPointer(){
		return leftPointer;
	}

	public Transform getRightPointer(){
		return rightPointer;
	}

	public Transform getEndPointer(){
		return endPointer;
	}

	public Transform getLeftLanePointer(){
		return leftLanePointer;
	}

	public Transform getRightLanePointer(){
		return rightLanePointer;
	}

	public Transform getMidLanePointer(){
		return midLanePointer;
	}

	/**
	 * Put back the platform to it's original state
	 */
	public void normalize(){
		foreach (GameObject go in children) {
			try{
				go.transform.parent = null;
				InfiniteObjectGenerator.getInstance().destroyObject (go);
			} catch(Exception e){
				Debug.Log (e.Message);
			}
		}
		clearChildren ();
		for (int i = 0; i < lastCollidable.Length; i++) {
			lastCollidable [i] = null;
		}
	}



	/*
	 * Try to retrieve the last GameObject positioned on that lane. Raise exception? Therefore no object has been spawned yet and return null;
	 */ 
	public GameObject getLastCollidable(int laneIndex){
		GameObject output;
		try{
			output = lastCollidable[laneIndex];
			output.GetComponents<CollidableObject>(); //Raise exception if not set yet.
			//Debug.Log ("Last collidable in lane " + laneIndex + ": " + output.name);
		}catch(Exception e){
			//Debug.Log ("No collidable spawned in lane " + laneIndex + " yet. This is pretty normal tho. Exception raised: " + e.Message);
			output = null;
		} 
		return output;
	}

	public void setLastCollidable(GameObject collidable,  int laneIndex){
		lastCollidable [laneIndex] = collidable;
	}

	public Vector2 getLaneXY(int laneIndex){
		Vector2 output;
		switch (laneIndex) {
		case 0:
			output = new Vector2 (leftLanePointer.transform.position.x, leftLanePointer.transform.position.y);
			break;
		case 1:
			output = new Vector2 (midLanePointer.transform.position.x, midLanePointer.transform.position.y);
			break;
		case 2:
			output = new Vector2 (rightLanePointer.transform.position.x, rightLanePointer.transform.position.y);
			break;
		default:
			Debug.LogError ("Dude WTF!");
			output = Vector3.one;
			break;
		}
		return output;
	}

	public float getMinZOffset(){
		return minZOffset;
	}

	/**
	 * Returns true if at least an hard costraint is violated
	 */ 
	public bool doesViolateCostraints(Vector3 positionToCheck, GameObject collidable, int laneIndex){
		Debug.Log ("Checking position " + positionToCheck);
		bool failure = false;
		Vector3 raycastDirection;
		int lanesBlocked = 0;
		RaycastHit[] hits, hits2, hits3; //Aux vars
		GameObject go;
		if (collidable.GetComponent<CollidableObject> ().isBlockingFullLane) {
			Debug.Log ("This obstacle is non avoidable");
			lanesBlocked = 1; 
			//Check if at least another lane is enabled.
			if(1 == getCollidableLanesEnabled().Length){
				Debug.Log ("Hard Constraint violated! Platform " + name + " has only 1 enabled lane and collidable " + collidable.name + " is not avoidable");
				failure = true;
			}
			//Check there is at least a free passage on the other lanes
			switch(laneIndex){ //Computes raycast direction
			case 0:
				//Left lane. Check right
				raycastDirection = rightLanePointer.transform.position - midLanePointer.transform.position;
				hits = Physics.RaycastAll (positionToCheck, raycastDirection, 20F);
				Debug.Log ("Found " + hits.Length + " gameObjects on " + collidable.name + "'s right");
				break;
			case 1:
				//Center lane. Check both left and right
				raycastDirection = rightLanePointer.transform.position - midLanePointer.transform.position;
				hits2 = Physics.RaycastAll (positionToCheck, raycastDirection, 20F);
				raycastDirection = leftLanePointer.transform.position - midLanePointer.transform.position;
				hits3 = Physics.RaycastAll (positionToCheck, raycastDirection, 20F);

				//Merge two arrays. I hate .NET
				hits = new RaycastHit[hits2.Length + hits3.Length];
				Array.Copy (hits2, hits, hits2.Length);
				Array.Copy (hits3, 0, hits, hits2.Length, hits3.Length);
				Debug.Log ("Found " + hits.Length + " gameObjects on " + collidable.name + "'s sides");
				break;
			case 2:
				//Right lane. Check left
				raycastDirection = leftLanePointer.transform.position - midLanePointer.transform.position;
				hits = Physics.RaycastAll (positionToCheck, raycastDirection, 20F);
				Debug.Log ("Found " + hits.Length + " gameObjects on " + collidable.name + "'s left");
				break;
			default:
				Debug.LogError ("DUDE WTF!");
				hits = new RaycastHit[0];
				break;
			}

			//Now check for other full blocking obstacles
			for (int i = 0; i < hits.Length; i++) {
				try{
					go = hits[i].collider.gameObject;
					if(go.GetComponent<InfiniteObject>().objectType == InfiniteObject.ObjectType.Collidable){
						if(go.GetComponent<CollidableObject>().collidableType == CollidableObject.CollidableType.Obstacle){
							if(go.GetComponent<CollidableObject>().isBlockingFullLane){
								//Found another obstacle blocking the full lane
								lanesBlocked++;
							}
						}
					}
				} catch(Exception e){
					Debug.LogError ("Maybe this is not an infiniteObject. This is rather normal.. Exception raised: " + e.Message);
				}
			}
			if (lanesBlocked == getCollidableLanesEnabled ().Length) {
				Debug.Log ("Hard Costraint violated! All lanes are blocked by non avoidable obstacles");
				failure = true;
			}

		}

		//Debug.Log ("Checking platform span:  collidable end point" + (positionToCheck.z + collidable.GetComponent<CollidableObject> ().getMaxBounds ().size.z) + ". Platform end: " + getMaxBounds ().max.z);
		Debug.Log ("Checking platform span:  collidable end point" + (collidable.transform.position.z + collidable.GetComponent<CollidableObject> ().getMaxBounds ().size.z) + ". Platform end: " + this.gameObject.transform.position.z + getMaxBounds ().size.z);
		//if (positionToCheck.z + collidable.GetComponent<CollidableObject> ().getMaxBounds ().size.z > getMaxBounds ().max.z) {
		if ((collidable.transform.position.z + collidable.GetComponent<CollidableObject> ().getMaxBounds ().size.z) > (this.gameObject.transform.position.z + getMaxBounds ().size.z)) {
			Debug.Log ("Hard Costraint violated! collidable end point: " + (positionToCheck.z + collidable.GetComponent<CollidableObject> ().getMaxBounds ().size.z) + " platform end: " + getMaxBounds ().max.z);
			failure = true; //The collidable spans outside the platform
		}

		if (!failure) {
			Debug.Log ("No costraints violated for ga");
		}
		return failure;
	}

	public bool supportThisCollidable(GameObject collidable){
		if (collidable.GetComponent<CollidableObject> ().canSpawnInCenterSlot && collidableCenterSpawn)
			return true;
		if (collidable.GetComponent<CollidableObject> ().canSpawnInLeftSlot && collidableLeftSpawn)
			return true;
		if (collidable.GetComponent<CollidableObject> ().canSpawnInRightSlot && collidableRightSpawn)
			return true;
		return false;
	}

}
