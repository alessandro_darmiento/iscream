﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleObject : CollidableObject
{
	//public readonly static ObjectType OBJECT_TYPE = ObjectType.Obstacle;


	private GameObject[] children;

	// Use this for initialization
	void Start ()
	{
		collidableType = CollidableType.Obstacle;
		GameObject coin;
		children = this.gameObject.GetComponents<GameObject> ();
		foreach(GameObject go in children){
			if (go.tag.Equals ("coinPlaceholder")) {
				coin = InfiniteObjectGenerator.getInstance ().createObject (Resources.Load<GameObject>("Prefab/Collidable/Coins/Coin")); //Get single coin prefab path
				coin.transform.SetParent(go.transform);
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	/**
	 * Turn the ObstacleObject back to it's original state
	 */ 
	public void normalize(){
		GameObject coin;
		foreach(GameObject go in children){
			if (go.tag.Equals ("coinPlaceholder")) {
				if (go.transform.childCount > 0) {
					coin = go.transform.GetChild (0).gameObject;
					coin.transform.parent = null;
					InfiniteObjectGenerator.getInstance ().destroyObject (coin);
				}
			}
		}
	}
}

