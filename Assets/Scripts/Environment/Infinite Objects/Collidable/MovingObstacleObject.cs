﻿using UnityEngine;
using System.Collections;

public class MovingObstacleObject : ObstacleObject
{
	private GameObject swallower;
	public float speed = 1;

	// Use this for initialization
	void Start ()
	{
		swallower = InfiniteObjectGenerator.getInstance ().swallower;
	}
	
	// Update is called once per frame
	void Update ()
	{	
		gameObject.transform.Translate(0, 0, swallower.transform.position.z*Time.deltaTime*speed);
	}
}

