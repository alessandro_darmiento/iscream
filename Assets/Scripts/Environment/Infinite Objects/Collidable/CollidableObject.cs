﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * A collidable object attaches itself to the platform when activated.
 */
public class CollidableObject : InfiniteObject {

	public enum CollidableType{Coin, Crystal, PowerUp, Obstacle, MovingObstacle}
	public CollidableType collidableType;

	public bool canSpawnInLeftSlot;
	public bool canSpawnInCenterSlot;
	public bool canSpawnInRightSlot;

	private PlatformObject platformParent;
	public bool isBlockingFullLane;
	private int lane = -1;


	public override void SetParent(Transform parent)
	{
		base.SetParent(parent);

		CollidableObject[] childCollidableObjects = null;
		for (int i = 0; i < thisTransform.childCount; ++i) {
			childCollidableObjects = thisTransform.GetChild(i).GetComponentsInChildren<CollidableObject>();
			for (int j = 0; j < childCollidableObjects.Length; ++j) {
				childCollidableObjects[j].SetStartParent(thisTransform.GetChild(i));
			}
		}
	}


	public override void Orient(PlatformObject parent, Vector3 position, Quaternion rotation)
	{
		base.Orient(parent, position, rotation);

		platformParent = parent;
	}

	
	// Use this for initialization
	void Start () {
		objectType = ObjectType.Collidable;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setLane(int lane){
		this.lane = lane;
	}

	public int getLane(){
		return lane;
	}

	public bool[] getCollidableLanesEnabled(){
		return new bool[3]{canSpawnInLeftSlot, canSpawnInCenterSlot, canSpawnInRightSlot};
	}

}
