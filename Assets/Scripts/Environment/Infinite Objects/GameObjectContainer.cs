﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace AssemblyCSharp
{

	/**
	 * This container allows to have a pool of spawned objects reference into a map with multiple value for each key 
	 */
	public class GameObjectContainer
	{
		//The maximum number of cached objects of the same type
		public static readonly int CACHE_LIMIT = 5;
		private string name;
		private List<GameObject> objectList;

		public GameObjectContainer (GameObject obj)
		{
			this.name = obj.name.Replace("(Clone)","");
			this.objectList = new List<GameObject> ();
			objectList.Add (obj);
		}

		public GameObject getObject(){
			GameObject gameObject = objectList [0];
			objectList.RemoveAt (0);
			return gameObject;
		}

		public List<GameObject> getObjectList(){
			return objectList;
		}

		public void pushGameObject(GameObject gameObject){
				objectList.Add(gameObject);
		}
	}
}

