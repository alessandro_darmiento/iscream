﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * The base InfiniteObject class. Each obj spawned in the game extends this class
 */
public class InfiniteObject : MonoBehaviour {

	public enum ObjectType {Platform, Collidable, Side}

	//Is this a platform, a collidable or...
	public ObjectType objectType;
	private Bounds bounds;
	private bool boundsExist = false;

	//Init values
	private Vector3 startPosition;
	private Quaternion startRotation;
	private Transform startParent;

	private InfiniteObject infiniteObjectParent;

	protected GameObject thisGameObject;
	protected Transform thisTransform;

	public virtual void Init()
	{
		startPosition = transform.position;
		startRotation = transform.rotation;
	}

	public virtual void Awake()
	{
		thisGameObject = gameObject;
		thisTransform = transform;
		startPosition = transform.position;
		startRotation = transform.rotation;
	}
		

	public Transform GetTransform()
	{
		return thisTransform;
	}

	public Vector3 GetStartPosition()
	{
		return startPosition;
	}

	public virtual void SetParent(Transform parent)
	{
		thisTransform.parent = parent;

		SetStartParent(parent);
	}

	public void SetStartParent(Transform parent)
	{
		startParent = parent;
	}

	public void SetInfiniteObjectParent(InfiniteObject parentObject)
	{
		infiniteObjectParent = parentObject;
		thisTransform.parent = parentObject.GetTransform();
	}

	public InfiniteObject GetInfiniteObjectParent()
	{
		return infiniteObjectParent;
	}

	// orient for platform and scene objects
	public virtual void Orient(Vector3 position, Quaternion rotation)
	{
		Vector3 pos = Vector3.zero;
		float yAngle = rotation.eulerAngles.y;
		pos.Set(startPosition.x * Mathf.Cos(yAngle * Mathf.Deg2Rad) + startPosition.z * Mathf.Sin(yAngle * Mathf.Deg2Rad), startPosition.y,
			-startPosition.x * Mathf.Sin(yAngle * Mathf.Deg2Rad) + startPosition.z * Mathf.Cos(yAngle * Mathf.Deg2Rad));
		pos += position;
		thisTransform.position = pos;
		thisTransform.rotation = startRotation;
		thisTransform.Rotate(0, yAngle, 0, Space.World);
	}


	// orient for collidables which have a platform as a parent
	public virtual void Orient(PlatformObject parent, Vector3 position, Quaternion rotation)
	{
		thisTransform.parent = parent.GetTransform();
		Vector3 pos = Vector3.zero;
		float yAngle = rotation.eulerAngles.y;
		pos.Set(startPosition.x * Mathf.Cos(yAngle * Mathf.Deg2Rad) + startPosition.z * Mathf.Sin(yAngle * Mathf.Deg2Rad), startPosition.y,
			-startPosition.x * Mathf.Sin(yAngle * Mathf.Deg2Rad) + startPosition.z * Mathf.Cos(yAngle * Mathf.Deg2Rad));
		pos += position;
		thisTransform.localPosition = parent.GetTransform().InverseTransformPoint(pos);
		thisTransform.rotation = startRotation;
		thisTransform.Rotate(0, rotation.eulerAngles.y, 0, Space.World);
	}


	public bool IsActive()
	{
		return thisGameObject.activeSelf;
	}


	/**
	 * Find gameObject bounds
	 */ 
	public Bounds getMaxBounds() {
		GameObject g = this.gameObject;
		if (false == boundsExist) {
			var b = new Bounds(g.transform.position, Vector3.zero);
			foreach (Renderer r in g.GetComponentsInChildren<Renderer>()) {
				b.Encapsulate(r.bounds);
			}
			bounds = b;
			boundsExist = true;
			return b;
		}
		return bounds;

	}


}
